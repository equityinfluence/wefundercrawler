package main

import (
	"bitbucket.org/equityinfluence/api/apimodels"
	"bitbucket.org/equityinfluence/crawler"
	"bytes"
	"crypto/md5"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	username string
	password string
)

// to run progam, follow these step:
//   cd path/to/realtysharescrawler/indexer
//   go build
//   ./indexer
func main() {
	var duration, concurency int
	flag.StringVar(&username, "username", "quipwag@gmail.com", "username to login to site")
	flag.StringVar(&password, "password", "Tom@friends1", "username to login to site")
	flag.IntVar(&duration, "d", 300, "duration to refetch company profile in second")
	flag.IntVar(&concurency, "c", 5, "number of worker run concurency")
	flag.Parse()

	linkCh := make(chan string)
	wg := &sync.WaitGroup{}

	// store cookie for request make by client
	// think client like a real web browser
	cookieJar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar: cookieJar,
	}

	// start worker, each worker work in concurence with each other.
	for i := 0; i < concurency; i++ {
		wg.Add(1)
		go worker(client, linkCh, wg)
	}

	// begin the job
	// keep doing again and never stop the program
	// sleep if have free time
	for {
		begin := time.Now()
		err := getProfileURLs(client, linkCh)
		if err != nil {
			log.Println("getProfileURLs:", err)
		}
		s := time.Second*time.Duration(duration) - time.Now().Sub(begin)
		if s > 0 {
			time.Sleep(s)
		}
	}
}

// in this example, a worker never stop too,
// it keep 'listen' on linkChan (link channel) for a new url to process
func worker(client *http.Client, linkChan chan string, wg *sync.WaitGroup) {
	// Decreasing internal counter for wait-group as soon as goroutine finishes
	defer wg.Done()

	for url := range linkChan {
		extractData(client, url)
	}
}

func loadPageNum(client *http.Client) int {
	url := "https://www.crowdfunder.com/crowdfunding/deals"
	resp, err := client.Get(url)

	//Check for the Login text in top right , if there is , we must login
	doc, err2 := goquery.NewDocumentFromResponse(resp)

	card_num := doc.Find(".js-preserve-filters").Last().Text()
	log.Println(err, err2)
	card_num = strings.Replace(card_num, "Deals (", "", -1)
	card_num = strings.Replace(card_num, ")", "", -1)

	card_num_int, err := strconv.ParseInt(card_num, 10, 0)
	if err != nil {
	}
	page := int(card_num_int)
	log.Println("card_num", card_num)
	page = page / 20
	return page
}

// this function use for login, login will post username/password to get cookie (client auto store that cookie)
// before login it need HTML content of the login page (and that HTML sotred in response object).
func login(client *http.Client, resp *http.Response) error {
	//log.Println("geting token")
	// load this page to get __RequestVerificationToken

	//doc, err := goquery.NewDocumentFromResponse(resp)
	//if err != nil {
	//    return err
	//}

	// .NET framework use this hidden input for some kind of security
	//token, ok := doc.Find("input[name='__RequestVerificationToken']").Last().Attr("value")
	//if !ok {
	//    return errors.New("cannot find __RequestVerificationToken value")
	//}

	data := url.Values{}
	data.Set("authenticity_token", "NJR43/aVNdbqFgGRBQkbFr2i1Jk4GOHJpkpOYPaP//0=")
	data.Set("user[email]", username)
	data.Set("user[password]", password)
	//data.Set("rememberMe", "true")
	//data.Set("__RequestVerificationToken", token)

	// this is the real login endpoint
	req, err := http.NewRequest("POST",
		"https://wefunder.me/sessions",
		strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}

	// fake a real browser request header
	req.Header.Set("Origin", "https://wefunder.me")
	req.Header.Set("Referer", "https://wefunder.me/login")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	log.Println("loggin in process...")
	_, err = client.Do(req)

	if err != nil {
		return err
	}

	return nil
}

// getProfileURLs extract profile url from an index page (that may include many urls)
func getProfileURLs(client *http.Client, linkChan chan string) error {

	log.Println("begin get comany url")

	login(client, nil)

	resp, err := client.Get("https://wefunder.me/companies")

	doc, errDoc := goquery.NewDocumentFromResponse(resp)


	if errDoc != nil {
		return err
	}

	//Check for the Login text in top right , if there is , we must login
	//LoginButton := doc.Find(".header-signin").First().Text()
	//if LoginButton != "" {
		//re-login
	//}

	log.Println("haha", err, errDoc)

	sel := doc.Find(".cardHolder_staff .info")
	for i := range sel.Nodes {
		single := sel.Eq(i)
		href, exist := single.Find("a").First().Attr("href")
		if !exist {
			// not found
			log.Println("not found")
		} else {
			log.Println("found url:", href, "start extracting")
			linkChan <- href
		}
	}



	return nil
}

func checkSocialLink(socialLink string) string {
	if strings.Contains(socialLink, "facebook") {
		return "facebook"
	}
	if strings.Contains(socialLink, "linkedin") {
		return "linkedin"
	}
	if strings.Contains(socialLink, "twitter") {
		return "twitter"
	}
	return ""
}

func formatMoney(money string) int {
	money = strings.TrimSpace(money)
	money = strings.Replace(money, "$", "", -1)
	money = strings.Replace(money, ",", "", -1)
	money_64, ok5 := strconv.ParseInt(money, 10, 0)
	log.Println(ok5)
	money_int := int(money_64)
	return money_int
}

// extractData extract data from an url
func extractData(client *http.Client, url string) error {
	fullurl := "https://wefunder.me" + url
	log.Println("begein extracting url:", fullurl)

	// this mean the sever redirect us to a new page.
	// maybe because we still not login
	resp, err := client.Get(fullurl)
	if err != nil {
		return err
	}

	if fullurl != resp.Request.URL.String() {
		err := login(client, resp)
		if err != nil {
			return err
		} else {
			resp, err = client.Get(url)
			if err != nil {
				return err
			}
		}
	}

	log.Println("begin parsing HTML")
	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return err
	}

	PIPR_Name :=doc.Find("h1").First().Text()
	PIPR_Name = strings.Replace(PIPR_Name, "Invest in", "", -1)
	PIPR_Name = strings.Trim(PIPR_Name,"\n")
	PIPR_Description := doc.Find(".elevator").Text()
	PIPR_Website ,errx:= doc.Find(".company_links a").Attr("href")
	Total_Invested_Raised := doc.Find(".funding_total").Text()
	Total_Invested_Raised = strings.Replace(Total_Invested_Raised,"raised","",-1)
	Total_Invested_Raised = strings.Replace(Total_Invested_Raised," ","",-1)
	Total_Invested_Raised_int := formatMoney(Total_Invested_Raised)
	Target_Raise := doc.Find(".fundraise_label").Text()
	Target_Raise = strings.Replace(Target_Raise,"Currently Fundraising","",-1)
	Target_Raise = strings.TrimSpace(Target_Raise)
	Target_Raise_int := -1
	if (strings.Index(Target_Raise,"Closing" ) > -1 ) {
		// WE SHOULD FIX THIS TARGET RAISE FOR INVESTMENT > 100%, THIS IS A DUMMY SOLUTION
		Target_Raise_int = Total_Invested_Raised_int
		log.Println("closing",Target_Raise_int)
	} else {
		Target_Raise = Target_Raise[:strings.Index(Target_Raise," goal")]
		Target_Raise_int = formatMoney(Target_Raise)
	}
	//Security_Type := doc.Find(".investment-type strong").Text()
	//City_State_Country := doc.Find(".company-location a").Text()
	//City_State_Country_array := strings.Split(City_State_Country, ", ")

	//var City, State, Country string
	//if len(City_State_Country_array) >= 1 {
		//City = City_State_Country_array[0]
	//}
	//if len(City_State_Country_array) >= 2 {
		//State = City_State_Country_array[1]
	//}
	//if len(City_State_Country_array) >= 3 {
		//Country = City_State_Country_array[2]
	//}
//
	//Industry := doc.Find(".company-tags .company-tag").Text()
	//MinInvestment_string_array := doc.Find("strong")
	//MinInvestment := 0
	//MinInvestment_string_array.Each(func(i int, s *goquery.Selection) {
		//if s.Text() == "Minimum Reservation" {
			//MinInvestment = formatMoney(s.Parent().Find("span").Text())
		//}
	//})
//
	//SocialLinks_1, ok1 := doc.Find(".company-social-media a").First().Attr("href")
	//SocialLinks_2, ok2 := doc.Find(".company-social-media a").Eq(1).Attr("href")
	//SocialLinks_3, ok3 := doc.Find(".company-social-media a").Eq(2).Attr("href")
//
	//socialType1 := checkSocialLink(SocialLinks_1)
	//socialType2 := checkSocialLink(SocialLinks_2)
	//socialType3 := checkSocialLink(SocialLinks_3)
//
	//SocialMediaURL := map[string]string{}
	//if socialType1 != "" {
		//SocialMediaURL[socialType1] = SocialLinks_1
	//}
	//if socialType2 != "" {
		//SocialMediaURL[socialType2] = SocialLinks_2
	//}
	//if socialType3 != "" {
		//SocialMediaURL[socialType3] = SocialLinks_3
	//}
	Logo, ok4 := doc.Find(".preload").First().Attr("src")
	Logo = "http:"+Logo
	Platform_logo := "https://phaven-prod.s3.amazonaws.com/files/image_part/asset/410996/6vWM8fZLqWcpYwQsILpn045SIUA/large_wefunder-logo.jpg"
//
	//OpenDate := ""
	//ClosingDate := ""
	//CompanySideBarUlLiStrong := doc.Find(".company-sidebar ul li strong")
	//CompanySideBarUlLiStrongTotal := CompanySideBarUlLiStrong.Size()
	//for i := 0; i < CompanySideBarUlLiStrongTotal; i++ {
		//if (CompanySideBarUlLiStrong.Eq(i).First().Text() == "Open Date" ) {
			//OpenDate = CompanySideBarUlLiStrong.Eq(i).Parent().Find("span").First().Text()
//
		//} else if (CompanySideBarUlLiStrong.Eq(i).First().Text() == "Closing Date" ){
			//ClosingDate = CompanySideBarUlLiStrong.Eq(i).Parent().Find("span").First().Text()
		//}
//
	//}


	//fullurl = strings.Replace(fullurl, "invest", "", -1)

	//log.Println("begein extracting url for team info:", fullurl)

	// this mean the sever redirect us to a new page.
	// maybe because we still not login
	resp, err = client.Get(fullurl)
	//if err != nil {
	//	return err
	//}

	//if fullurl != resp.Request.URL.String() {
	//	err := login(client, resp)
	//	if err != nil {
	//		return err
	//	} else {
	//		resp, err = client.Get(url)
	//		if err != nil {
	//			return err
	//		}
	//	}
	//}

	//log.Println("begin parsing HTML")
	//doc, err = goquery.NewDocumentFromResponse(resp)
	//if err != nil {
	//	return err
	//}
	//PIPR_Description := doc.Find(".custom-content p").Text()
	//ok5:=false
	//CompanyLeaderImage:=""

	leaders := []map[string]string{}
	member:= map[string]string{}
	TotalCompanyLeader := doc.Find(".founder").Size()

	for i := 0; i < TotalCompanyLeader; i++ {

		//CompanyLeaderImage,ok5 = doc.Find(".founder").Eq(i).Find("img").First().Attr("src")
		//CompanyLeaderImage = "https://" + CompanyLeaderImage
		member = map[string]string{}
		member["name"] = doc.Find(".founder").Eq(i).Find(".name").First().Text()
		member["title"] =doc.Find(".founder").Eq(i).Find("h5").First().Text()
		member["bio"] = doc.Find(".founder").Eq(i).Find(".bio i").First().Text()
		//member["avatar"] = CompanyLeaderImage
		leaders = append(leaders,member)
	}
	//log.Println(ok5)
	for i := 0; i < TotalCompanyLeader; i++ {
		log.Println("haha leaders  ",leaders[i])
	}

	//log.Println(ok1, ok2, ok3, ok4,ok5, Platform_logo)
//
	log.Println("PIPR_Name ", PIPR_Name)
	log.Println("PIPR_Website ", PIPR_Website)
	log.Println("PIPR_Description ", PIPR_Description)
	log.Println("Total_Invested_Raised ", Total_Invested_Raised_int)
	log.Println("Target_Raise ", Target_Raise_int)
	//log.Println("Security_Type ", Security_Type)
	//log.Println("City ", City)
	//log.Println("State ", State)
	//log.Println("Country ", Country)
	//log.Println("Industry ", Industry)
	//log.Println("SocialMediaURL", SocialMediaURL)
	log.Println("Logo", Logo)
	//log.Println("MinInvestment", MinInvestment)
	//log.Println("OpenDate ", OpenDate)
	//log.Println("ClosingDate ", ClosingDate)
//
	log.Println(errx,ok4)
	h := md5.New()
	h.Write([]byte(fullurl))
	portalHashHex := fmt.Sprintf("%x", h.Sum(nil))
	siteName := "wefunder.me"
//
	//// TODO(nvcnvn): handle Security_Type and Sector at enum
	company := apimodels.Company{}
	company.Portal = &apimodels.Portal{}
	company.PortalHash = &portalHashHex
	company.Portal.Link = &fullurl
	company.Portal.Name = &siteName
	company.Name = &PIPR_Name
	company.Description = &PIPR_Description
	//company.City = &City
	//company.State = &State
	//company.Country = &Country
	//company.Industry = &Industry
	//company.SocialMediaURL = SocialMediaURL
	company.Logo = &Logo
	company.TotalInvestedRaised = &Total_Invested_Raised_int
	company.TargetRaise = &Target_Raise_int
	//company.MinInvestment = &MinInvestment
	company.PlatformLogo = &Platform_logo
	//// need to parse string to time
	////company.ReportedStartDate = &OpenDate
	////company.ClosingDate = &ClosingDate

	buff := bytes.Buffer{}
	err = json.NewEncoder(&buff).Encode(&company)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", crawler.API_ENDPOINT, &buff)
	if err != nil {
		return err
	}

	req.Header.Set("X-Authorization", "n5hoTEXQ61741HTN1KJMJfa8coDyhAz0YpC9wk1WUBqNoZkNtBBg5S9X8sTffYg")

	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New(resp.Status)
	}

	return nil
}
